# If you come from bash you might have to change your $PATH.
export PATH=$HOME/bin:/usr/local/bin:$PATH
export PATH=$PATH:/opt/texlive/2019/bin/x86_64-linux
# Path to your oh-my-zsh installation.
export ZSH="/home/fox/.oh-my-zsh"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="robbyrussell"

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in ~/.oh-my-zsh/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to automatically update without prompting.
# DISABLE_UPDATE_PROMPT="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS=true

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in ~/.oh-my-zsh/plugins/*
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git)

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

. "${HOME}/.cache/wal/colors.sh"
export PATH="${PATH}:${HOME}/.local/bin/"
(cat ~/.cache/wal/sequences &)
[[ $- != *i* ]] && return
vim_fzf() {
vim "$(fzf)"
}
alias cs='cowsay'
alias gb='cd ~/repos/great-bait/source'
alias ct='cowthink'
alias dmen='dmenu_run -nb "$color0" -nf "$color15" -sb "$color1" -sf "$color15"'
alias inst='sudo xbps-install -S'
alias delt='sudo xbps-remove -R'
alias srch='sudo xbps-query -Rs'
alias ls='ls --color=auto'
alias ll='ls -la'
alias wp='ranger ~/Pictures/Wallpapers'
alias rn='ranger'
alias feh='feh --scale-down --auto-zoom'
alias bedit='vim ~/.bashrc'
alias vedit='vim ~/.config/nvim/init.vim'
alias i3edit='vim ~/.config/i3/config'
alias ttefc='cd ~/repos/ttef/ttefc/'
alias ufetch='bash ~/repos/readonly/ufetch/ufetch-void'
alias cs='cowsay'
alias v='vim'
alias cm='cd ..'
alias 4c='ranger ~/Pictures/4chan/'
alias cdp='cd ~/Pictures'
alias wedit='sudo vim /etc/wpa_supplicant/wpa_supplicant-wlp3s0.conf'
alias cdd='cd ~/Documents/'
alias dl='youtube-dl'
alias adl='youtube-dl -x --audio-format flac --audio-quality 0 --add-metadata'
alias am='alsamixer'
alias cdm='cd ~/Music/'
alias cdv='cd ~/Videos/'
alias mktex='cp ~/template $(pwd)/template.tex'
alias htop='htop -t'
alias ftp='ftp -i'
alias rftp='ftp 192.168.2.114'
alias rrftp='ftp $ipaddrraspi'
alias redit='vim ~/.config/ranger/rc.conf'
alias lacon='sudo pvpn -l'
alias ccon='sudo pvpn -cc'
alias discon='sudo pvpn -d'
alias p2p='sudo pvpn -p2p'
alias vstat='sudo pvpn --status'
alias recon='sudo pvpn --reconnect'
alias cl='clear'
alias cdr='cd ~/repos/'
alias theme='wal --theme'
alias rmconf='rm ~/.config/zathura/zathurarc'
alias cdpu='cd /etc/portage/package.use/'
alias svim='sudo vim'
alias z='zathura'
alias xv='cd ~/Pictures/4chan/stuff/vids'
alias inst='sudo xbps-install -S'
alias delt='sudo xbps-remove -R'
alias srch='xbps-query -Rs'
alias ghci='ghci-color'
alias tedit='vim ~/todo'
alias tp='cp ~/Documents/latemp.tex'
alias H='cd ~/repos/Haskell/'
alias tmux="env TERM=xterm-256color tmux"
alias nm="neomutt"
alias n='ncmpcpp'
. /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
. /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
setxkbmap -layout us -option ctrl:nocaps
export PATH=$PATH:/usr/local/texlive/2019/bin/x86_64-linux
